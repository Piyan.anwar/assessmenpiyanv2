package buybackstorageservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
	"time"
)

type DataBuyback struct {
	Gram  float64 `json:"gram"`
	Harga int     `json:"harga"`
	Norek string  `json:"norek"`
}


func Buyback(data []byte) {
	var arrdata []byte
	var dataBuy DataBuyback
	tanggal := makeTimestamp()
	for i:=0;i<len(data)-1;i++{
		arrdata=append(arrdata, data[i])
	}
	err := json.Unmarshal(arrdata, &dataBuy)
	if err != nil {
		fmt.Println("marshal",err.Error())
	}
	saldoNow := GetSaldo(dataBuy.Norek)
	db := config.ConnDB()
	defer db.Close()
	sum := saldoNow - dataBuy.Gram
	// fmt.Println(sum)
	sql := "insert into tbl_buyback (gram,harga,norek_id)values($1,$2,$3)"
	_, errsql := db.Exec(sql, dataBuy.Gram, dataBuy.Harga, dataBuy.Norek)
	if errsql != nil {
		fmt.Println("BuybackErr:", errsql)
	}
	// update saldo rekening
	UpdSaldo(sum,dataBuy.Norek)
	// insert tabel transaksi
	RecordTransaksi(dataBuy.Norek, tanggal, "buyback", dataBuy.Gram, sum)
}

func GetSaldo(norek string) float64 {
	var saldo float64
	db := config.ConnDB()
	defer db.Close()
	sql := "select saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql, norek)
	if err != nil {
		fmt.Println("errorGetSaldo:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&saldo)
	}
	// fmt.Println("gs", saldo)
	return saldo
}
func UpdSaldo(saldo float64, norek string) {
	db := config.ConnDB()
	defer db.Close()
	sql := "update tbl_rekening set saldo=$1 where norek=$2"
	_, err := db.Exec(sql, saldo, norek)
	if err != nil {
		fmt.Println("updateRekErr:", err)
	}
}

func RecordTransaksi(norek string,dates int64,tipe string,gram float64,saldo float64){
	db := config.ConnDB()
	defer db.Close()
	sql:="Insert into tbl_transaksi(norek_id,date,type,gram,harga_id,saldo) values ($1,$2,$3,$4,1,$5)"
	_,err:=db.Exec(sql,norek,dates,tipe,gram,saldo)
	if err!=nil{
		fmt.Println("RecordT:",err)
	}
}


func makeTimestamp() int64 {
    return time.Now().UnixNano() / int64(time.Millisecond)
}