package topupservices

import (
	"encoding/json"
	"fmt"
	"math"
	"microservices/config"
	topupservicesstorage "microservices/topup-services-storage"
	"net/http"
	"strconv"

	"github.com/teris-io/shortid"
)

type DataTopup struct {
	Gram  string `json:"gram"`
	Harga string `json:"harga"`
	Norek string `json:"norek"`
}

type responBerhasil struct {
	Res    bool `json:"error"`
	Ref_id string `json:"reff_id"`
}
type responGagal struct {
	Res     bool `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}

func Topup(w http.ResponseWriter, r *http.Request){
	topic:="topup"
	partition:=0
	var data DataTopup
	resultSukses:=responBerhasil{}
	resultGagal:=responGagal{}
	refid, _ := shortid.Generate()
	err:=json.NewDecoder(r.Body).Decode(&data)
	if err!=nil{
		fmt.Println("jsonErrTopup:",err)
	}
	dkafka,_:=json.Marshal(data)
	saldoNow := GetHargaNow()
	hargaConv, _ := strconv.Atoi(data.Harga)
	gramConv, _ := strconv.ParseFloat(data.Gram, 32)
	// fmt.Println(gramConv)
	gramRound := roundFloat(gramConv, 3)
	if data.Norek == "r001" && data.Norek != "" && saldoNow == hargaConv && gramRound > 0.001 {
		config.PublishKafka(topic,partition,dkafka)
		dataConsume:=config.ConsumeKafka(topic,partition)
		topupservicesstorage.TopUp(dataConsume)
		
		resultSukses=responBerhasil{
			Res: false,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(resultSukses)
	} else {
		resultGagal=responGagal{
			Res: true,
			Ref_id: refid,
			Message: "Kafka Not Ready",
		}
		json.NewEncoder(w).Encode(resultGagal)
	}
}
func roundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}

func GetHargaNow() int {
	var data int
	db := config.ConnDB()
	defer db.Close()
	sql := "select topup from tbl_harga"
	row, err := db.Query(sql)
	if err != nil {
		fmt.Println("getnowhargaerr:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&data)
	}
	return data
}
