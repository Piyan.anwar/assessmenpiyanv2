package topupservicesstorage

import (
	"encoding/json"
	"fmt"
	"math"
	"microservices/config"
	"strconv"
	"time"
)

type DataTopup struct {
	Gram  string `json:"gram"`
	Harga string `json:"harga"`
	Norek string `json:"norek"`
}

func roundFloat(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}

// func makeTimestamp() int64 {
// 	return time.Now().UnixNano() / int64(time.Millisecond)
// }

func TopUp(data []byte) {
	var daTopup DataTopup
	tanggal := makeTimestamp()
	err := json.Unmarshal(data, &daTopup)
	if err != nil {
		fmt.Println(err.Error())
	}
	db := config.ConnDB()
	gramConv, _ := strconv.ParseFloat(daTopup.Gram, 32)
	gramRound := roundFloat(gramConv, 3)
	hargaConv, _ := strconv.Atoi(daTopup.Harga)
	defer db.Close()
	saldo := GetSaldo(daTopup.Norek)
	sum := saldo + gramRound
	sumBulat := roundFloat(sum, 3)
	sql := "insert into tbl_topup (gram,harga,norek_id) values($1,$2,$3)"
	_,errsql := db.Exec(sql, gramRound, hargaConv, daTopup.Norek)
	if errsql != nil {
		fmt.Println("TopupErr:", errsql)
	}
	// update salod rekening
	UpdSaldo(sumBulat, daTopup.Norek)
	// insert tabel transaksi
	RecordTransaksi(daTopup.Norek, tanggal, "topup", gramRound, sumBulat)
}

func UpdSaldo(saldo float64, norek string) {
	db := config.ConnDB()
	defer db.Close()
	sql := "update tbl_rekening set saldo=$1 where norek=$2"
	_, err := db.Exec(sql, saldo, norek)
	if err != nil {
		fmt.Println("updateRekErr:", err)
	}
}

func GetSaldo(norek string) float64 {
	var saldo float64
	db := config.ConnDB()
	defer db.Close()
	sql := "select saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql, norek)
	if err != nil {
		fmt.Println("errorGetSaldo:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&saldo)
	}
	// fmt.Println("gs", saldo)
	return saldo
}

func GetHargaNow() int {
	var data int
	db := config.ConnDB()
	defer db.Close()
	sql := "select topup from tbl_harga"
	row, err := db.Query(sql)
	if err != nil {
		fmt.Println("getnowhargaerr:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&data)
	}
	return data
}

func RecordTransaksi(norek string,dates int64,tipe string,gram float64,saldo float64){
	db := config.ConnDB()
	defer db.Close()
	sql:="Insert into tbl_transaksi(norek_id,date,type,gram,harga_id,saldo) values ($1,$2,$3,$4,1,$5)"
	_,err:=db.Exec(sql,norek,dates,tipe,gram,saldo)
	if err!=nil{
		fmt.Println("RecordT:",err)
	}
}

func makeTimestamp() int64 {
    return time.Now().UnixNano() / int64(time.Millisecond)
}