package inputhargaservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
	inputhargastorageservices "microservices/input-harga-storage-services"
	"net/http"

	"github.com/teris-io/shortid"
)

type UpdHarga struct {
	Admin_id string `json:"admin_id"`
	Harga    int    `json:"harga_topup"`
	Buyback  int    `json:"harga_buyback"`
}

type responBerhasil struct {
	Res    bool `json:"error"`
	Ref_id string `json:"reff_id"`
}
type responGagal struct {
	Res     bool `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}

func InsertHarga(w http.ResponseWriter, r *http.Request) {
	topic:="input-harga"
	partition:=0
	var hargainput UpdHarga
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&hargainput)
	kafPublish,_:=json.Marshal(hargainput)
	if err != nil {
		fmt.Println("error con:", err)
	}
	result := responBerhasil{}
	result2 := responGagal{}
	id:=hargainput.Admin_id
	if id != "a001" || id == ""  {
		result2 = responGagal{
			Res:     true,
			Ref_id:  refid,
			Message: "Kafka Not Ready",
		}
		json.NewEncoder(w).Encode(result2)
	} else {
		config.PublishKafka(topic,partition,kafPublish)
		kafConsume:=config.ConsumeKafka(topic,partition)
		inputhargastorageservices.InsertHarga(kafConsume)
		result = responBerhasil{
			Res:    false,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(result)
	}
}