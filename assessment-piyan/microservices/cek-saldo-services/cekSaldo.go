package ceksaldoservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
	"net/http"

	"github.com/teris-io/shortid"
)

type rekening struct {
	Norek string `json:"norek"`
}

type Saldo struct {
	Norek string  `json:"norek"`
	Saldo float32 `json:"saldo"`
}

type ResponCekSaldo struct {
	Res   bool `json:"error"`
	Saldo Saldo  `json:"data"`
}

type responGagal struct {
	Res     bool   `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}

func CekSaldoRekening(w http.ResponseWriter, r *http.Request) {
	topic:="cek-saldo"
	partition:=0
	var noRek rekening
	rfid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&noRek)

	if err != nil {
		fmt.Println("errcekConn:", err)
	}
	responf := responGagal{}
	result := SaldoRekening(noRek.Norek)
	kafPublish,_:=json.Marshal(noRek)
	if result.Saldo.Saldo != 0 {
		config.PublishKafka(topic,partition,kafPublish)
		json.NewEncoder(w).Encode(result)
	} else {
		responf = responGagal{
			Res:     true,
			Ref_id:  rfid,
			Message: "Kafka Not Ready",
		}
		json.NewEncoder(w).Encode(responf)
	}
}


func SaldoRekening(norek string) ResponCekSaldo {
	var respon ResponCekSaldo
	db := config.ConnDB()
	defer db.Close()
	sql := "select norek,saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql,norek)
	if err != nil {
		fmt.Println("errorSaldo:", err)
	}
	defer row.Close()
	respon = ResponCekSaldo{
		Res: false,
	}
	for row.Next() {
		row.Scan(&respon.Saldo.Norek, &respon.Saldo.Saldo)
	}
	return respon
}
