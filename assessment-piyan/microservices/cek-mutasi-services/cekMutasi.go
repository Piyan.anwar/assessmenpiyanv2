package cekmutasiservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
	"net/http"

	"github.com/teris-io/shortid"
)

type getTransaksi struct {
	Norek     string `json:"norek"`
	DateStart int    `json:"start_date"`
	DateEnd   int    `json:"end_date"`
}
type transaksi struct {
	Tanggal int64 `json:"date"`
	Tipe    string	`json:"type"`
	Gram    float64 `json:"gram"`
	Harga   int `json:"harga-topup"`
	Buyback int `json:"harga-buyback"`
	Saldo   float64 `json:"saldo"`
}
type resTransaksi struct {
	Res  bool      `json:"error"`
	Data []transaksi `json:"data"`
}
type responGagal struct {
	Res     bool `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}


func GetMutasi(w http.ResponseWriter, r *http.Request) {
	topic:="mutasi"
	partition:=0
	var data getTransaksi
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		fmt.Println("errMutasi:", err)
	}
	kafkaPublish,_:=json.Marshal(data)
	result := GetMutasiModels(data)
	if result.Data == nil {
		responf := responGagal{
			Res:     true,
			Ref_id:  refid,
			Message: "Kafka NotReady",
		}
		json.NewEncoder(w).Encode(responf)
	} else {
		config.PublishKafka(topic,partition,kafkaPublish)
		json.NewEncoder(w).Encode(result)
	}
}


func GetMutasiModels(data getTransaksi)resTransaksi{
	var result resTransaksi
	var dataTr []transaksi
	db:=config.ConnDB()
	defer db.Close()
	sql:="select date,type,gram,topup,buyback,saldo from tbl_transaksi t join tbl_harga h on h.id=t.harga_id where t.norek_id=$1 and date between $2 and $3"
	row,err:=db.Query(sql,data.Norek,data.DateStart,data.DateEnd)
	if err!=nil{
		fmt.Println("MutasiErr:",err)
	}
	defer row.Close()
	for row.Next(){
		var rows transaksi
		err=row.Scan(&rows.Tanggal,&rows.Tipe,&rows.Gram,&rows.Harga,&rows.Buyback,&rows.Saldo)
		if err!=nil{
			fmt.Println("errScan:",err)
		}
		dataTr=append(dataTr, rows)
	}
	result=resTransaksi{
		Res: false,
		Data: dataTr,
	}
	return result
}