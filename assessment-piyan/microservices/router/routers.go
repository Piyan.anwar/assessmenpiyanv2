package router

import (
	buybackservices "microservices/buyback-services"
	cekhargaservices "microservices/cek-harga-services"
	cekmutasiservices "microservices/cek-mutasi-services"
	ceksaldoservices "microservices/cek-saldo-services"
	inputhargaservices "microservices/input-harga-services"
	topupservices "microservices/topup-services"

	"github.com/gorilla/mux"
)

func Routers() *mux.Router {
	rute := mux.NewRouter()
	publicrute := rute.PathPrefix("/api").Subrouter()
	publicrute.HandleFunc("/topup", topupservices.Topup).Methods("POST")
	publicrute.HandleFunc("/input-harga",inputhargaservices.InsertHarga).Methods("POST")
	publicrute.HandleFunc("/cek-harga",cekhargaservices.CekHarga).Methods("GET")
	publicrute.HandleFunc("/saldo",ceksaldoservices.CekSaldoRekening).Methods("POST")
	publicrute.HandleFunc("/buyback",buybackservices.Buyback).Methods("POST")
	publicrute.HandleFunc("/mutasi",cekmutasiservices.GetMutasi).Methods("POST")

	return rute
}
