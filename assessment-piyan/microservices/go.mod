module microservices

go 1.19

require github.com/gorilla/handlers v1.5.1

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	golang.org/x/tools v0.1.11-0.20220513221640-090b14e8501f // indirect
)

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.7
	github.com/segmentio/kafka-go v0.4.38
	github.com/teris-io/shortid v0.0.0-20220617161101-71ec9f2aa569
	honnef.co/go/tools v0.3.3
)
