package inputhargastorageservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
)

type UpdHarga struct {
	Admin_id string `json:"admin_id"`
	Harga    int    `json:"harga_topup"`
	Buyback  int    `json:"harga_buyback"`
}

func InsertHarga(data []byte) {
	var dataHarga UpdHarga
	err := json.Unmarshal(data, &dataHarga)
	if err != nil {
		fmt.Println(err.Error())
	}
	db := config.ConnDB()
	defer db.Close()
	sql := "update tbl_harga set topup=$1, buyback=$2"
	_, errsql := db.Exec(sql, dataHarga.Harga, dataHarga.Buyback)
	if errsql != nil {
		fmt.Println("QueryInsertErr:", err)
	}
}
