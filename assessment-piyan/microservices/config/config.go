package config

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/segmentio/kafka-go"
)

func ConnDB() *sql.DB {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Error ENV:", err)
	}

	db, err := sql.Open("postgres", os.Getenv("POSTGRES_URL"))
	if err != nil {
		fmt.Println("Error Database:", err)
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("Koneksi DB Error:", err)
	}
	return db
}

func PublishKafka(topic string, partition int, data []byte) {

	conn, err := kafka.DialLeader(context.Background(), "tcp", "localhost:9092", topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	conn.SetWriteDeadline(time.Now().Add(20 * time.Second))
	_, err = conn.WriteMessages(
		kafka.Message{Value: data},
	)
	if err != nil {
		log.Fatal("failed to write messages:", err)
		
	}

	if err := conn.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}
}

func ConsumeKafka(topic string, partition int) []byte{

	conn, err := kafka.DialLeader(context.Background(), "tcp", "localhost:9092", topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	conn.SetReadDeadline(time.Now().Add(5 * time.Second))
	batch := conn.ReadBatch(10e3, 1e6) // fetch 10KB min, 1MB max

	b := make([]byte, 10e3) // 10KB max per message
	for {
		_, err := batch.Read(b)
		if err != nil {
			break
		}
		
	}

	// if err := batch.Close(); err != nil {
	// 	log.Fatal("failed to close batch:", err)
	// }

	if err := conn.Close(); err != nil {
		log.Fatal("failed to close connection:", err)
	}

	var arrdata []byte
	for i := 0; i < len(b); i++ {
		if b[i] != 0 {
			arrdata = append(arrdata, b[i])
		} else {
			break
		}
	}

	return arrdata
}
