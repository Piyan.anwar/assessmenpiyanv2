package buybackservices

import (
	"encoding/json"
	"fmt"
	buybackstorageservices "microservices/buyback-storage-services"
	"microservices/config"
	"net/http"

	"github.com/teris-io/shortid"
)

type DataBuyback struct {
	Gram  float64 `json:"gram"`
	Harga int     `json:"harga"`
	Norek string  `json:"norek"`
}

type responBerhasil struct {
	Res    bool `json:"error"`
	Ref_id string `json:"reff_id"`
}
type responGagal struct {
	Res     bool `json:"error"`
	Ref_id  string `json:"reff_id"`
	Message string `json:"message"`
}


func Buyback(w http.ResponseWriter, r *http.Request) {
	topic:="buyback"
	partition:=0
	var dataBuy DataBuyback
	refid, _ := shortid.Generate()
	err := json.NewDecoder(r.Body).Decode(&dataBuy)
	if err != nil {
		fmt.Println("buybackErr:", err)
	}
	saldoNow:=GetSaldo(dataBuy.Norek)
	kafkaPublish,_:=json.Marshal(dataBuy)
	if dataBuy.Norek == "r001" && dataBuy.Norek != "" && saldoNow < dataBuy.Gram {
		responf := responGagal{
			Res:     true,
			Ref_id:  refid,
			Message: "Kafka Not Ready",
		}
		json.NewEncoder(w).Encode(responf)
	} else {
		config.PublishKafka(topic,partition,kafkaPublish)
		kafkaConsume:=config.ConsumeKafka(topic,partition)
		buybackstorageservices.Buyback(kafkaConsume)
		respons := responBerhasil{
			Res:    false,
			Ref_id: refid,
		}
		json.NewEncoder(w).Encode(respons)
	}

}

func GetSaldo(norek string) float64 {
	var saldo float64
	db := config.ConnDB()
	defer db.Close()
	sql := "select saldo from tbl_rekening where norek=$1"
	row, err := db.Query(sql, norek)
	if err != nil {
		fmt.Println("errorGetSaldo:", err)
	}
	defer row.Close()
	for row.Next() {
		row.Scan(&saldo)
	}
	// fmt.Println("gs", saldo)
	return saldo
}