package cekhargaservices

import (
	"encoding/json"
	"fmt"
	"microservices/config"
	"net/http"
)


type Harga struct{
	Buyback  int    `json:"harga_buyback"`
	Harga    int    `json:"harga_topup"`
}

type ResponCekHarga struct{
	Res bool `json:"error"`
	Data Harga `json:"data"`
}


func CekHarga(w http.ResponseWriter, r *http.Request) {
	cekHarga, _ := json.MarshalIndent(CekHargaModel(), "", "\t")
	w.Write(cekHarga)
}

func CekHargaModel()ResponCekHarga{
	var dataharga ResponCekHarga
	db:=config.ConnDB()
	defer db.Close()
	sql:="select topup,buyback from tbl_harga"
	row,err:=db.Query(sql)
	if err!=nil{
		fmt.Println("QCekErr:",err)
	}
	defer row.Close()
	for row.Next(){
		dataharga=ResponCekHarga{
			Res: false,
		}
		row.Scan(&dataharga.Data.Harga,&dataharga.Data.Buyback)
	}
	return dataharga
}