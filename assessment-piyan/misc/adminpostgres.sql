-- Adminer 4.8.1 PostgreSQL 15.1 (Debian 15.1-1.pgdg110+1) dump

DROP TABLE IF EXISTS "tbl_buyback";
DROP SEQUENCE IF EXISTS tbl_buyback_id_seq;
CREATE SEQUENCE tbl_buyback_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tbl_buyback" (
    "id" integer DEFAULT nextval('tbl_buyback_id_seq') NOT NULL,
    "gram" numeric,
    "harga" integer,
    "norek_id" character varying(10),
    CONSTRAINT "tbl_buyback_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_harga";
DROP SEQUENCE IF EXISTS tbl_harga_id_seq;
CREATE SEQUENCE tbl_harga_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tbl_harga" (
    "id" integer DEFAULT nextval('tbl_harga_id_seq') NOT NULL,
    "topup" integer,
    "buyback" integer,
    CONSTRAINT "tbl_harga_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_rekening";
DROP SEQUENCE IF EXISTS tbl_rekening_id_seq;
CREATE SEQUENCE tbl_rekening_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tbl_rekening" (
    "id" integer DEFAULT nextval('tbl_rekening_id_seq') NOT NULL,
    "norek" character varying(10),
    "saldo" numeric,
    CONSTRAINT "tbl_rekening_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_topup";
CREATE TABLE "public"."tbl_topup" (
    "id" integer NOT NULL,
    "gram" numeric,
    "harga" integer,
    "norek_id" character varying(10)
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_transaksi";
CREATE TABLE "public"."tbl_transaksi" (
    "id" integer NOT NULL,
    "norek_id" character varying(10),
    "date" bigint,
    "type" text,
    "gram" numeric,
    "harga_id" integer DEFAULT '1',
    "saldo" numeric
) WITH (oids = false);


-- 2022-11-23 10:04:08.163755+00
